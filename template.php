<?php


/**
 * Modify theme variables
 */
function phptemplate_preprocess(&$vars) {
  global $user;                                            // Get the current user
  $vars['is_admin'] = in_array('admin', $user->roles);     // Check for Admin, logged in
	$vars['logged_in'] = ($user->uid > 0);
}

function phptemplate_preprocess_page(&$vars) {
	$body_classes = array();
	$body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';// Page useris logged in
	$body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';      // Page is ront page
	$body_classes[] = ($vars['search_box']) ? 'search' : 'search_no';

	$vars['body_classes'] = implode(' ', $body_classes);

	// Page change based on node->type
	// Add a new page-TYPE template to the list of templates used
	if (isset($vars['node'])) {
			// Add template naming suggestion. It should alway use hyphens.
			$vars['template_files'][] = 'page-'. str_replace('_', '-', $vars['node']->type);   
	}

	$vars['mission'] = theme_get_setting('mission', false);
	$vars['slogan'] = t(variable_get('site_slogan', ''));

	//$vars['closure'] = theme('closure');
	//	print '<pre>';
	//	print_r($vars);
	//	print '</pre>';
}

/**
	* Override or insert PHPTemplate variables into the search_theme_form template.
	*
	* @param $vars
	*   A sequential array of variables to pass to the theme template.
	* @param $hook
	*   The name of the theme function being called (not used in this case.)
*/
function phptemplate_preprocess_search_theme_form(&$vars, $hook) {
	//print_r($vars);
	// Modify elements of the search form
	$vars['form']['search_theme_form']['#title'] = '';

	// Set a default value for the search box
	$vars['form']['search_theme_form']['#value'] = t('Search: ');

	// Add a custom class to the search box
	$vars['form']['search_theme_form']['#attributes'] = array('class' => 'txtSearch s',
			'onfocus' => "if (this.value == 'Search: ') {this.value = '';}",
			'onblur' => "if (this.value == '') {this.value = 'Search: ';}");

	// Change the text on the submit button
	//$vars['form']['submit']['#value'] = t('Go');

	// Rebuild the rendered version (search form only, rest remains unchanged)
	unset($vars['form']['search_theme_form']['#printed']);
	$vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

	$vars['form']['submit']['#type'] = 'image_button';
	$vars['form']['submit']['#src'] = path_to_theme() . '/images/go.gif';
	//$vars['form']['submit']['#attributes'] += array('class' => t('noborder') );

	// Rebuild the rendered version (submit button, rest remains unchanged)
	unset($vars['form']['submit']['#printed']);
	$vars['search']['submit'] = drupal_render($vars['form']['submit']);

	// Collect all form elements to make it easier to print the whole form.
	$vars['search_form'] = implode($vars['search']);
}
?>
