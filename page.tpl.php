<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body class="<?php print $body_classes; ?>">
<center>
  <div id="page">
		<div id="header_wrapper">
		<div id="header_top">
			<div id="header_title">
				<?php if (!empty($site_name)): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
				<?php print $site_name; ?> | <span id="mission"><?php  print $mission; ?></span>
				</a>
				<?php endif; ?>
			</div>
			 <?php if ($slogan): ?>
				<div id="slogan"><?php print $slogan; ?></div>
			<?php endif; ?>
		</div>
		<div id="header_bottom">
			<div id="menu">
				<div id="menu_pad">
					<div id="primary-menu-wrapper" class="clearfix">
					<?php if (isset($primary_links)) : ?>
						<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
					<?php endif; ?>
						<?php if (isset($secondary_links)) : ?>
						<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
					<?php endif; ?>

					</div><!-- /primary-menu-wrapper -->
					
					<div id="menu_search_box">
					<?php if ($search_box) : ?>
						<div id="search-box">
						<?php print $search_box; ?>
						</div><!-- /search-box -->
					<?php endif; ?>

					<!--form method="get" id="searchform" style="display:inline;" action="<?php $front_page ?>/">
							<span>Search:&nbsp;</span>
							<input type="text" class="s" value="<?php  ?>" name="s" id="s" />&nbsp;
							<input type="image" src="/sites/all/themes/0deta/images/go.gif" value="Submit" class="sub"/>
					</form-->
					</div>
				</div> <!--menupad-->
			</div><!--menu-->
		</div><!--header_bottom-->
		</div><!--header_wrapper-->
	<!--/div--><!--page-->
		<div id="main-wrapper">
			<div id="main-left">
				<?php if ($sidebar_first): ?>
				<div id="sidebar-first">
				<?php print $sidebar_first; ?>
				</div><!-- /sidebar-first -->
				<?php endif; ?>
			</div> <!--main-left-->
			<div id="main" class="clearfix">

			<?php if ($breadcrumb): ?>
				<!--div id="breadcrumb">
				<?php print $breadcrumb; ?>
				</div--><!-- /breadcrumb -->
			<?php endif; ?>

				<div id="content-wrapper">
					<?php if ($content_top): ?>
					<div id="content-top">
          	<?php print $content_top; ?>
					</div>
        	<?php endif; ?>
          
					<?php if ($tabs): ?>
          <div id="content-tabs">
             <?php print $tabs; ?>
          </div>
          <?php endif; ?>
					<?php if ($messages): ?>
					<?php print $messages; ?>
					<?php endif; ?>
					<div id="content">

						<div id="content-inner">
						<?php if ($help): ?>
							<div id="help">
								<?php print $help; ?>
							</div>
						<?php endif; ?>

						<?php if ($title): ?>
							<h1 class="title"><?php print $title; ?></h1>
						<?php endif; ?>
							<div id="content-content">
							<?php print $content; ?>
							</div>
						</div> <!--content-inner-->
					</div> <!--content-->

				</div> <!--content-wrapper-->
				<div id="main-right">
				<?php if ($sidebar_last): ?>
				<div id="sidebar-last">
					<?php print $sidebar_last; ?>
				</div><!-- /sidebar_last -->
				<?php endif; ?>
				</div>
			</div> <!--main-->
		</div> <!--main-wrapper-->
	</div><!--page-->
	<?php if ($footer_top || $footer || $footer_message): ?>
	<div id="footer" class="clearfix">
			<?php if ($footer_top): ?>
				<div id="footer_top">
				<?php print $footer_top; ?>
				</div>
			<?php endif; ?>
			
				
			<?php if ($footer): ?>
				<div id="footer-content">
				<?php print $footer; ?>
				</div>
			<?php endif; ?>
			
			<?php if ($footer_message): ?>
				<div id="footer_message">
				<?php print $footer_message; ?>
				</div>
			<?php endif; ?>
	</div><!-- /footer -->
	<?php endif; ?>
</center>
	<?php print $closure; ?>
</body>
</html>
